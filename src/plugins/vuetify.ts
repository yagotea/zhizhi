import Vue from "vue";
import Vuetify from "vuetify/lib";
import colors from "vuetify/es5/util/colors";

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: "mdi"
  },
  theme: {
    themes: {
      light: {
        primary: "#4778BD",
        secondary: "#FA7A35",
        accent: "#3187C1",
        error: "#E73828"
      }
    }
  }
});
